clean: clean_pyc
clean_pyc:
	find . -type f -name "*.pyc" -delete || true

run:
	python quartz/app.py

tests:
	python quartz/test/tests.py
