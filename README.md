# Implementation Notes 

* It's a [Flask](http://flask.pocoo.org/) app with a [Flask-SQLAlchemy](https://pythonhosted.org/Flask-SQLAlchemy/) managed database.
* Please install the requirements needed first from the requirements.txt located in the requirements folder of the project or install the development.txt requirements
in the same folder as it's going to install also the live requirements as well.
* For development purposes, this is using docker resources for the database.
* This implements the Swagger (Open API implementation).

## Notes:
* You should have a virtualenv in PYTHON 3.6+ for the requirements and isolation.
* For the module postgres, you need to change your SQLALCHEMY_DATABASE_URI to `SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://.....'`. If you use databases, which we are not but we could!
* The requirements files are structured for development and live system inside the folder `requirements` and the command to run on dev is `pip install -r requirements/development.txt`
* The file structure contains the settings of the project, tests, views, requirements and resources used for these tasks
* You should always export the `PYTHONPATH` in the root of the project before triggering the server

## Notes 2:
* Is not mandatory to run `docker-compose up -d` as we don't use DBs for this task but just to show that we were able to add if we needed to.
* As mentioned before, you should always export the `PYTHONPATH` in the root of the project before triggering the server

```shell
export PYTHONPATH=`pwd`
```
* You should have docker installed if you would like to run `docker-compose up` but this is OPTIONAL

## Before start
* Start the docker server `docker-compose up` to start possible databases or ignore it and run `make run` as we don't use any DB for this test

## Makefile
* To make it easier to start the local server a Makefile was created so it's just needed to run `make run` and it will automatically start the dev server

## Getting Started - Running locally
You should have docker installed to run some containers for the DB and Redis.

```shell
docker volume create --name=gousto_db_data
docker-compose up or docker-compose up -d
```

- The containers should start normally

## The endpoints can be seen using the following urls
```shell script
http://localhost:5001/
```

- Accessing the URL should give you the OpenAPI UI to test the APIs

## Configuration Settings

Create your own settings file and symbolic link to the manage command-
```Shell
[tiago@tiagos gousto]$ cd quartz/settings/
[tiago@tiagos settings]$ cp example_config.py local_config_ti.py 
[tiago@tiagos settings]$ ln -s local_config_ti.py local_config.py
```

## Run Unittests

```Shell
[tiago@tiagos gousto]$ export PYTHONPATH=`pwd`
[tiago@tiagos gousto]$ nosetests --exe

```

Or alternatively `make tests`

## Start Application

Then run it
```Shell
[tiago@tiagos gousto]$ export PYTHONPATH=`pwd`
[tiago@tiagos gousto]$ cd quartz/
[tiago@tiagos gousto]$ python app.py
```
 * Running on http://127.0.0.1:5001/
 