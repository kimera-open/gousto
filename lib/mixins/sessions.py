"""
@author: tiago
"""
from flask import current_app


class DBSession(object):
    """
    Returns the session of a Given DB passed
    """
    @property
    def session(self):
        app = current_app.extensions.get('sqlalchemy', None)
        if not app:
            return
        return app.db.session

    def get_object(self, id=None, model=None):
        """
        Returns a single object based on ID
        :param id: Id of the object
        :param model: Id of the object
        :return:
        """
        return self.session.query(model).get(id)

    def get_queryset(self, id=None, model=None):
        """
        Return a queryset with the results if not ID is passed
        :param id: Id of the object
        :param model: Id of the object
        :return: queryset
        """
        if not id:
            return self.session.query(model).all()
        return self.get_object(id, model)
