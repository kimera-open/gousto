"""
@author: tiago
"""
from flask_restplus import Api
from quartz.views import (IntelligencePackingOrderApiView, IntelligencePackingSmallestBoxApiView,
                          IntelligencePackingApiView,NoIntelligencePackingApiView, OrderBoxApiView)


def routes(app):
    """
    Function that will generate all the views and blueprints and corresponding endpoints.
    The reason why API_URL is called is because this property can be set on deployment time any time.
    Args:
        :param app: The Flask Object
    """
    # TOGGLE TO GOOD API DESIGN
    api = Api(app=app, version="2.0", title="Gousto Intelligence Packing APIs", description="Manages the CRUD")
    namespace = api.namespace("Gousto", description="Gousto Intelligence Packing APIs")

    # APPLY THE ROUTES
    namespace.add_resource(IntelligencePackingOrderApiView,
                           '/api/v1/orders/<order_id>', endpoint='intelligent-packing')
    namespace.add_resource(IntelligencePackingSmallestBoxApiView,
                           '/api/v1/orders/box/<order_id>', endpoint='ip-smallest-box')

    # GENERAL
    namespace.add_resource(IntelligencePackingApiView, '/api/v1/intelligent-packing/', endpoint='ip-all')
    namespace.add_resource(NoIntelligencePackingApiView, '/api/v1/no-intelligent-packing/', endpoint='ip-all-none')
    namespace.add_resource(OrderBoxApiView, '/api/v1/order-boxes/', endpoint='order-boxes')
