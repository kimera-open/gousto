import os
from quartz.settings.global_config import BaseConfig


class Config(BaseConfig):
    DEBUG = True
    AUTO_RELOADER = False
    DATABASE_USER = os.environ.get('POSTGRES_USER', 'root')
    DATABASE_PASSWORD = os.environ.get('POSTGRES_PASSWORD', '')
    DATABASE_NAME = os.environ.get('POSTGRES_DB', 'gousto')
    SQLALCHEMY_DATABASE_URI = "postgresql://{}:{}@localhost/{}".format(DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME)

    # API Versioning and URI
    API_PREFIX = "/quartz/api"
    API_VERSION = "/v1"
    API_URL = API_PREFIX + API_VERSION
