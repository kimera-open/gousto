import os


class BaseConfig(object):
    DEBUG = False
    APP_TITLE = "Quartz"
    AUTO_RELOADER = False
    
    # API Versioning and URI    
    API_PREFIX = ""
    API_VERSION = ""
    API_URL = API_PREFIX + API_VERSION

    # SQL ALCHEMY
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = ""

    DATABASE_USER = os.environ.get('POSTGRES_USER', 'postgres')
    DATABASE_PASSWORD = os.environ.get('POSTGRES_PASSWORD', 'postgres')
    DATABASE_NAME = os.environ.get('POSTGRES_DB', 'gousto')
    SQLALCHEMY_DATABASE_URI = "postgresql://{}:{}@localhost/{}".format(DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME)

    # GENERAL CONFIGS
    JSONIFY_PRETTYPRINT_REGULAR = 2

    # BASIC AUTH
    BASIC_AUTH_USERNAME = os.environ.get('BASIC_AUTH_USERNAME', 'admin')
    BASIC_AUTH_PASSWORD = os.environ.get('BASIC_AUTH_PASSWORD', 'gousto')
