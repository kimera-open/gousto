"""
All the utils of the application
"""
import json
from operator import attrgetter

from quartz.generics import Box, Order, OrderBox
from quartz.helpers import load_boxes, load_orders


def is_valid_json(str_data):
    """
    Receives a str and validates if is valid JSON format

    Args:
    :param: (str): Data to be validated if is a valid JSON format or not

    :return: boolean if is a valid JSON or not
    """
    try:
        json.loads(str_data)
    except ValueError:
        return False
    return True


def get_box_by_id(_id, payload=None):
    """
     Returns a box by ID
    :param _id: Id of the box
    :param payload: Payload to get the data
    :return: Box object
    """
    _payload = payload or load_boxes()
    return Box.get_by_id_or_none(_id, _payload)


def get_order_by_id(_id, payload=None):
    """
    Returns an order by ID
    :param _id: Id of the order
    :param payload: Payload to get the data
    :return: Order object
    """
    _payload = payload or load_orders()
    return Order.get_by_id_or_none(_id, _payload)


def get_boxes(payload=None):
    """
    Get all the boxes from a payload
    :param payload: Payload to get the data
    :return: List of boxes
    """
    _payload = payload or load_boxes()
    return Box.get_list_or_none(_payload)


def get_orders(payload=None):
    """
    Get all the boxes from a payload
    :param payload: Payload to get the data
    :return: List of boxes
    """
    _payload = payload or load_orders()
    return Order.get_list_or_none(_payload)


def get_boxes_per_order(order_id):
    """
    Returns the boxes for a given order based on the volume of that same box
    1. Gets all the available boxes
    2. Gets the order
    3. Choose the box
    :param order_id: ID of the order
    :return: Box
    """
    order = get_order_by_id(order_id)
    if not order:
        return
    return [box for box in get_boxes() if order.volume_cm3 <= box.volume_cm3]


def get_smallest_box_per_order(order_id):
    """
    Returns the smallest box for a given order based on the volume of that same box
    1. Gets all the available boxes
    2. Gets the order
    3. Choose the box
    :param order_id: ID of the order
    :return: Box
    """
    boxes = get_boxes_per_order(order_id)
    if not boxes:
        return
    return min(boxes, key=attrgetter('volume_cm3'))


def get_all_boxes_per_order_id():
    """
    Returns all the matching boxes per order id
    :return: boxes
    """
    orders = get_orders()
    return [OrderBox(order=order, **{'box_list': get_boxes_per_order(order.id)}) for order in orders]


def get_largest_box_per_order(order_id):
    """
    Returns the largest box for a given order based on the volume of that same box
    1. Gets all the available boxes
    2. Gets the order
    3. Choose the box
    :param order_id: ID of the order
    :return: Box
    """
    boxes = get_boxes_per_order(order_id)
    if not boxes:
        return
    return max(boxes, key=attrgetter('volume_cm3'))


def get_smallest_boxes_for_all_orders():
    """
    Returns the smallest box for all the orders
    """
    orders = get_orders()
    boxes = [OrderBox(box=get_smallest_box_per_order(order.id), order=order) for order in orders]
    return boxes


def get_largest_boxes_for_all_orders():
    """
    Returns the largest box for all the orders
    """
    orders = get_orders()
    boxes = [OrderBox(box=get_largest_box_per_order(order.id), order=order) for order in orders]
    return boxes
