"""
All the helpers and utils are placed here
"""
import json


def load_boxes():
    """
    Loads the information about the boxes
    :return: json
    """
    with open("resources/boxes.json", 'r') as f:
        boxes = json.load(f)
    return boxes


def load_orders():
    """
    Loads the information about the orders
    :return: json
    """
    with open("resources/orders.json", 'r') as f:
        orders = json.load(f)
    return orders
