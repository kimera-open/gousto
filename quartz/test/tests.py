"""
@author: tiago
"""
import json
import unittest

import quartz.generics
import quartz.utils
from quartz.helpers import load_orders, load_boxes


class Application(unittest.TestCase):
    """
    Tests the Platform
    """

    def setUp(self):
        self.orders = load_orders()
        self.boxes = load_boxes()

    def test_orders_are_not_empty(self):
        """Loads the orders without errors from the file"""
        self.assertIsNotNone(self.orders)

    def test_boxes_are_not_empty(self):
        """Loads the boxes without errors from the file"""
        self.assertIsNotNone(self.boxes)

    def test_can_create_box_objects(self):
        """Creates box objects"""
        boxes = quartz.utils.get_boxes(self.boxes)

        self.assertEqual(type(boxes[0]), quartz.generics.Box)
        self.assertIsNotNone(boxes)
        self.assertEqual(3, len(boxes))

    def test_can_create_order_objects(self):
        """Creates order objects"""
        orders = quartz.utils.get_orders(self.orders)

        self.assertEqual(type(orders[0]), quartz.generics.Order)
        self.assertIsNotNone(orders)
        self.assertEqual(10, len(orders))

    def test_can_create_order_boxes(self):
        """Can create an association between orders and boxes"""
        order_boxes = quartz.utils.get_all_boxes_per_order_id()

        self.assertEqual(type(order_boxes[0]), quartz.generics.OrderBox)
        self.assertIsNotNone(order_boxes)
        self.assertEqual(10, len(order_boxes))

    def test_order(self):
        """Tests the data of a given order id"""
        order = quartz.utils.get_order_by_id(1, self.orders)

        self.assertIsNotNone(order)
        self.assertEqual(order.id, str(1))
        self.assertEqual(5, len(order.ingredients))

    def test_box(self):
        """Tests the data for a given box"""
        box = quartz.utils.get_box_by_id("PK-MED-01")

        self.assertIsNotNone(box)
        self.assertEqual(box.id, "PK-MED-01")
        self.assertEqual(box.name, "Medium")
        self.assertEqual(30, box.dimensions.widthMm)
        self.assertEqual(50, box.dimensions.heightMm)
        self.assertEqual(60, box.dimensions.depthMm)

    def test_can_create_order_box_only(self):
        """With a given order and box, creates an object"""
        order = quartz.utils.get_order_by_id(1, self.orders)
        box = quartz.utils.get_box_by_id("PK-MED-01")

        order_box = quartz.generics.OrderBox(order=order, box=box)

        self.assertIsNotNone(order_box)
        self.assertEqual(order_box.order.id, order.id)
        self.assertEqual(order_box.box.name, box.name)
        self.assertEqual(order_box.box.id, box.id)

    def test_get_largest_boxes_for_all_orders(self):
        """Return 10 boxes for 10 orders"""
        order_boxes = quartz.utils.get_largest_boxes_for_all_orders()

        for order_box in order_boxes:
            self.assertEqual(order_box.box.name, "Large")
            self.assertEqual(order_box.box.id, "PK-LRG-03")

    def test_get_smallest_box_per_order(self):
        """Return the smallest box for a given order"""
        box = quartz.utils.get_smallest_box_per_order(1)

        self.assertEqual(box.name, "Medium")
        self.assertEqual(box.id, "PK-MED-01")


class UtilsTest(unittest.TestCase):
    """
    Tests some utils used in the application
    """

    def test_is_not_valid_json(self):
        json_test = "{'test': 1, 'test2': 'thing}"

        is_valid = quartz.utils.is_valid_json(json_test)

        self.assertFalse(is_valid)

    def test_is_valid_json(self):
        json_test = json.dumps({"test": 1, "test2": "thing"})

        is_valid = quartz.utils.is_valid_json(json_test)

        self.assertTrue(is_valid)

    def test_slot(self):
        """Tests if a dict can be parsed to a slot object"""
        test = {'obj': 1, 'pro': 'property', 'val': 3}

        slot = quartz.generics.GoustoObject(test)

        self.assertTrue(isinstance(slot, quartz.generics.GoustoObject))
        self.assertEqual(test.get('obj'), slot.obj)
        self.assertEqual(test.get('pro'), slot.pro)
        self.assertEqual(test.get('val'), slot.val)


if __name__ == "__main__":
    unittest.main()
