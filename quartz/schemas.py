"""
@author: tiago
"""

from marshmallow import Schema, fields


class OrderSchema(Schema):
    """
    /api/ - POST
    Parameters:
        - id (list)
        - ingredients (list)
    The `required` param ensures the field exists
    """

    id = fields.Integer(required=True)
    ingredients = fields.List(required=True, cls_or_instance=fields.Str(), allow_none=False)


class BoxSchema(Schema):
    """
    Representation of a Box
    """
    name = fields.String(required=True)
    co2FootprintKg = fields.Integer(required=True)


class OrderBoxSchema(Schema):
    """
    Representation of an order with a associated boxes
    """
    id = fields.Integer(required=True)
    box = fields.Nested(BoxSchema, required=False)
    total_co2 = fields.Integer()


class OrderListBoxSchema(Schema):
    """
    Representation of an order with a associated boxes
    """
    id = fields.Integer(required=True)
    box_list = fields.List(fields.Nested(lambda: BoxSchema()))
    saved_lorry = fields.Boolean()
    total_co2 = fields.Integer()
