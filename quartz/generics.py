"""
All the generic objects and functions might live here
"""
from quartz.exceptions import BoxDimensionException


class GoustoObject(object):
    """
    Class that converts a dict into a object using slots.
    Performance wise, is faster and gives a better memory usage.
    This class provides a nested setattr.

    Usage:
        _dict = {'a': 1, 'b': {'c': 1}}
        s = SlotObject(_dict)

    Result:
        s.a
        1

        s.b.c
        1
    """
    __slots__ = ['__dict__']

    def __init__(self, __dict__, name=None):
        self.name = name
        self.order_id = None
        for k, v in __dict__.items():
            setattr(self, k, self.__class__(v)) if isinstance(v, dict) else setattr(self, k, v)

    def __getitem__(self, item):
        return getattr(self, item)

    def __repr__(self):
        return '<%s: %s />' % (self.__class__.__name__, self.name)

    @classmethod
    def get_by_id_or_none(cls, _id, payload):
        """
        Returns a given object by ID or none
        :param Klass: Type of object to return
        :param _id: ID to lookup
        :param payload: Payload to iterate
        :return: object
        """
        _object = [cls(klass, name=klass.get('id', None)) for klass in payload if str(klass.get('id')) == str(_id)]
        if not _object:
            return
        return _object[0]

    @classmethod
    def get_list_or_none(cls, payload):
        """
        Gets an iterable based on the class and payload
        :return iterable: Volume of all boxes
        """
        return [cls(klass, name=klass.get('name', klass.get('id', None))) for klass in payload]


class Box(GoustoObject):
    """
    Representation of a Box object
    """

    @property
    def volume_mm3(self):
        """
        Returns the volume in mm3
        :return:
        """
        try:
            return self.dimensions.widthMm * self.dimensions.heightMm * self.dimensions.depthMm
        except BoxDimensionException:
            raise BoxDimensionException("There is no attributes matching the criteria")

    @property
    def volume_cm3(self):
        """
        Calculates the volume of a given object and converts from MM to CM
        :return:
        """
        try:
            return (self.dimensions.widthMm / 10) * (self.dimensions.heightMm / 10) * (self.dimensions.depthMm / 10)
        except BoxDimensionException:
            raise BoxDimensionException("There is no attributes matching the criteria")

    @property
    def volume_m3(self):
        """
        Returns the volume in m3
        :return:
        """
        try:
            return (self.dimensions.widthMm / 100) * (self.dimensions.heightMm / 100) * (self.dimensions.depthMm / 100)
        except BoxDimensionException:
            raise BoxDimensionException("There is no attributes matching the criteria")


class Order(GoustoObject):
    """
    Representation of an Order
    """

    @property
    def volume_mm3(self):
        """
        Returns the volume in mm3
        :return: Volume
        """
        _values = [volume.get('volumeCm3') for volume in self.ingredients]
        return sum(_values) * 10

    @property
    def volume_cm3(self):
        """
        Calculates the volume of a specific order in Cm3
        :return: Volume
        """
        _values = [volume.get('volumeCm3') for volume in self.ingredients]
        return sum(_values)

    @property
    def volume_m3(self):
        """
        Calculates the volume of a specific order in m3
        :return: Volume
        """
        _values = [volume.get('volumeCm3') for volume in self.ingredients]
        return sum(_values) / 10


class OrderBox:
    """
    Builds a relation between a box and an Order
    """

    def __init__(self, order, box=None, **kwargs):
        try:
            self.id = int(order.id)
        except TypeError:
            self.id = order.id
        self.box = box
        self.order = order
        self.box_list = kwargs.get('box_list')

    def __call__(self, *args, **kwargs):
        pass

    def __repr__(self):
        if self.box:
            return '<%s: %s - %s />' % (self.__class__.__name__, self.box.name, self.order.id)
        return '<%s: %s />' % (self.__class__.__name__, self.order.id)

    @property
    def total_co2(self):
        """Returns the total CO2"""
        if self.box:
            return self.box.co2FootprintKg
        return sum([box.co2FootprintKg for box in self.box_list])
