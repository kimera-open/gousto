"""
@author: tiago
"""
from flask import Flask, jsonify

from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_basicauth import BasicAuth


def create_app(settings_class):
    """
    Quartz - Flask object is created
    
    Args:
        :param settings_class: The object settings for the application
    
    Returns:
        :return: The Flask Object
    """
    app = Flask(__name__)
    app.config.from_object(settings_class)

    db = SQLAlchemy(app)
    marshmallow = Marshmallow(app)
    basic = BasicAuth(app)

    # INITIALIZE THE COMPONENTS FOR THE APP
    db.init_app(app)
    basic.init_app(app)
    marshmallow.init_app(app)

    # Generate the views
    from quartz.routes import routes
    routes(app)
    
    return app


if __name__ == '__main__':
    app = create_app('settings.local_config.Config')
    app.run(debug=app.config['DEBUG'], use_reloader=app.config['AUTO_RELOADER'], port=5001)
