"""
@author: tiago
"""
from flask import jsonify, make_response
from flask_api import status
from flask_restplus import Resource

import quartz.utils
from quartz.schemas import OrderSchema, BoxSchema, OrderBoxSchema, OrderListBoxSchema


class IntelligencePackingOrderApiView(Resource):
    """
    Returns the information about an Order
    """
    schema = OrderSchema()

    def get(self, order_id):
        """
        Return List of current order in the system
        :param order_id: ID of the order
        """
        order = quartz.utils.get_order_by_id(order_id)
        if not order:
            return make_response(jsonify(f"There is no order with the ID {order_id}"), status.HTTP_404_NOT_FOUND)
        return self.schema.dump(order)


class IntelligencePackingSmallestBoxApiView(Resource):
    """
    Returns the smallest Box for a given order using Intelligence Packing
    """
    schema = BoxSchema()

    def get(self, order_id):
        """
        Return the smallest box for a given order
        :param order_id: ID of the order
        """
        box = quartz.utils.get_smallest_box_per_order(order_id)
        if not box:
            return make_response(jsonify(f"There are no boxes available or is not a valid order id"))
        return self.schema.dump(box)


class IntelligencePackingApiView(Resource):
    """
    Returns all the smallest boxes for all the orders in the system
    """
    schema = OrderBoxSchema()

    def get(self):
        """
        Returns all the smallest boxes for all the orders in the system
        """
        order_boxes = quartz.utils.get_smallest_boxes_for_all_orders()
        if not order_boxes:
            return make_response(jsonify(f"There are no boxes to show"))
        return self.schema.dump(order_boxes, many=True)


class NoIntelligencePackingApiView(Resource):
    """
    Returns all the largest boxes for all the orders in the system
    """
    schema = OrderBoxSchema()

    def get(self):
        """
        Returns all the largest boxes for all the orders in the system
        """
        order_boxes = quartz.utils.get_largest_boxes_for_all_orders()
        if not order_boxes:
            return make_response(jsonify(f"There are no boxes to show"))
        return self.schema.dump(order_boxes, many=True)


class OrderBoxApiView(Resource):
    """
    Returns all the matching boxes for all the orders
    """
    schema = OrderListBoxSchema()

    def get(self):
        """
        All the boxes matching the order ids
        """
        order_boxes = quartz.utils.get_all_boxes_per_order_id()
        if not order_boxes:
            return make_response(jsonify(f"There are no boxes to show"))
        return self.schema.dump(order_boxes, many=True)
